# -*- coding: utf-8 -*-
#!/usr/bin/env python
"""
    una_health_data_challenge.utils
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    This module implements utilities

    :copyright: (c) 2021 by Leonardo Lucio Custode.
    :license: MIT, see LICENSE for more details.
"""
import re
import numpy as np


# Define variables that will be used in all the scripts
class Variables:
    GLUCOSE_VAR = "Glucose Level mg/dL"
    GLUCOSE_TIME_VAR = "Gerätezeitstempel"
    ACTIVITY_TIME_VAR = "timestamp_start"
    ACTIVITY_MEAL_TYPE = "record_type"
    DESCRIPTION = "description"
    PHYS_ACTIVITY_STRING = "ACTVITY"
    INITIAL_GLUCOSE = "initial_glucose"
    FINAL_GLUCOSE = "final_glucose"
    MEAL_TYPE = "meal_type"
    MEAL_START_TIME = "meal_start_time"
    GLUCOSE_MEASUREMENT = "Glukosewert-Verlauf mg/dL"
    GLUCOSE_SCAN = "Glukose-Scan mg/dL"
    WINDOW_SIZE = 3600 * 3
    GRANULARITY = 900
    RECORDING_TYPES = [0, 1]
    DF_MULTI_NAME = "multi_patient_df.csv"
    DF_MLEVELS_NAME = "meal_levels.pkl"
    DIFF = "diff"
    MEAN = "mean"


def get_nearest_glucose_level(levels, seconds):
    """
    Returns the glucose level measured most closely to
    the specified date and time.
    It interpolates linearly between the two closest points.

    :levels: The dataframe containing the glucose levels.
                Note: the dataframe must contain the column
                "Glucose Level mg/dL"
    :seconds: The datetime in seconds (int)
    :returns: A float
    """
    times = levels[Variables.GLUCOSE_TIME_VAR].values
    dist = np.abs(times - seconds)
    indices = np.argsort(dist)
    p1 = levels.loc[indices[0], Variables.GLUCOSE_VAR]
    p2 = levels.loc[indices[1], Variables.GLUCOSE_VAR]
    return (p1 + p2) / 2


def get_sliced_glucose_level(levels, time1, time2):
    """
    Returns the time series of the glucose level contained between two time points

    :levels: The dataframe containing the glucose levels.
                Note: the dataframe must contain the column
                "Glucose Level mg/dL"
    :time1: The initial time
    :time2: The final time
    :returns: TODO
    """
    times = levels[Variables.GLUCOSE_TIME_VAR].values
    dist = np.abs(times - time1)
    start = np.argmin(dist)
    dist = np.abs(times - time2)
    stop = np.argmin(dist)
    while stop < len(times) and times[stop] - times[start] <= Variables.WINDOW_SIZE:
        # If possible, get the followin sample, so that
        #   we include the level after 3 hrs
        stop += 1
    return levels.loc[start:stop, [Variables.GLUCOSE_VAR, Variables.GLUCOSE_TIME_VAR]]


def preprocess_text_foods(string):
    """
    Divides a string containing a list of foods (divided by ",", "+" or "&")
    and returns it as a list of strings.

    :string: A string containing foods consumed
    :returns: A list of strings
    """
    """
    Make it lowercase, to ensure compatibility between
      different records
    """
    desc = string.lower()
    # Adopt a single convention to divide the elements
    desc = desc.replace("&", ",")
    desc = desc.replace("+", ",")
    desc = desc.replace("-", ",")
    # Remove abbreviations
    desc = desc.replace("etw.", "")
    # Remove unnecessary parts
    desc = desc.replace("%", "")
    desc = desc.replace("(", " ")
    desc = desc.replace(")", " ")
    desc = desc.replace("/", " ")
    # Remove quantitites
    desc = desc.replace("ml", "")
    desc = desc.replace(" g ", "")
    desc = re.sub("[0-9]", "", desc)
    # Remove spaces
    desc = desc.replace(" ", "")
    return desc.split(",")


def interpolate(df, time_points):
    """
    Interpolates the glucose levels from the dataframe
    and returns a series that contains the glucose values
    for only the specified time points.

    :df: A DataFrame containing the relative time and the
                glucose level
    :time_points: A list of time points to use
    :returns: A list of values
    """
    values = []
    times = df[Variables.GLUCOSE_TIME_VAR].values
    times -= times.min()
    times.sort()
    times = np.array(times, dtype=int)

    for t in time_points:
        if t in times:
            # We do not need to interpolate
            values.append(
                df.loc[
                    df[Variables.GLUCOSE_TIME_VAR] == t,
                    Variables.GLUCOSE_VAR
                ].values[0]
            )
        else:
            # We need to interpolate
            left, right = None, None

            for t_idx, time in enumerate(times):
                if t > time and left is None:
                    left = t_idx
                if t < time and right is None:
                    right = t_idx
                if (right is not None and left is None) or \
                   (left is not None and right is not None):
                    break

            """
            Understand if we can use inverse-distance weighting or we
            need to use first-order hold
            """
            if left is not None and right is not None:
                # Inverse-distance weighting
                # Compute distances
                dist_left = t - times[left]
                dist_right = times[right] - t

                # Compute weights
                w_l = 1/dist_left
                w_r = 1/dist_right

                # weighting
                new_value = w_l * df[Variables.GLUCOSE_VAR].values[left]
                new_value += w_r * df[Variables.GLUCOSE_VAR].values[right]
                new_value /= (w_l + w_r)
            else:
                # First-order hold
                if left is not None:
                    new_value = df[Variables.GLUCOSE_VAR].values[left]
                else:
                    new_value = df[Variables.GLUCOSE_VAR].values[right]
            values.append(new_value)
    return values
