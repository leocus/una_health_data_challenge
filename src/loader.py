# -*- coding: utf-8 -*-
#!/usr/bin/env python
"""
    una_health_data_challenge.loader
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    This module implements the Patient class and loading methods

    :copyright: (c) 2021 by Leonardo Lucio Custode.
    :license: MIT, see LICENSE for more details.
"""
import os
import pickle
import pandas as pd
from utils import Variables
from utils import get_nearest_glucose_level, Variables, get_sliced_glucose_level, \
        preprocess_text_foods


class Patient:
    """
    This class represents a patient, and contains their glucose
    level and their activity
    """

    def __init__(self, glucose_level, activity):
        """
        Initializes a new instance

        :glucose_level: A DataFrame containing the glucose level
        :activity: A DataFrame containing the activity of the patient
        """
        self._glucose_level = glucose_level
        self._activity = activity

    def get_glucose_level(self):
        return self._glucose_level

    def set_glucose_level(self, value):
        self._glucose_level = value

    def get_activity(self):
        return self._activity

    def set_activity(self, value):
        self._activity = value

    @staticmethod
    def load_glucose_data(patient_dir, preprocessor):
        """
        This method loads the glucose-related data from the
        directory of the patient.

        :patient_dir: The path to the directory of the patient
        :preprocessor: A Preprocessor object
        :returns: A DataFrame
        """
        level_path = os.path.join(patient_dir, "levels_all.csv")

        """
        Compute how many rows we have to skip for the levels file, since the
          Number of lines before the header is not constant
        """
        to_skip = 0
        with open(level_path) as infile:
            for line_idx, content in enumerate(infile):
                if content.split(",")[0] == "Gerät":
                    # We reached the header
                    to_skip = line_idx
                    break

        levels = pd.read_csv(level_path, skiprows=to_skip)
        levels = preprocessor.apply(levels)
        return levels

    @staticmethod
    def load_activity_data(patient_dir, preprocessor):
        """
        This method loads and preprocesses the data related to the
        activities of the patient

        :patient_dir: The path to the directory of the patient
        :preprocessor: A Preprocessor object
        :returns: A DataFrame
        """
        activities_path = os.path.join(patient_dir, "activities_all.csv")
        """
        The files associated to the meals do not have any problem related
        to the initial rows
        """
        activities = pd.read_csv(activities_path)
        activities = preprocessor.apply(activities)
        return activities

    @staticmethod
    def load_patient_data(patient_dir, glucose_preprocessor, activity_preprocessor):
        """
        Loads the data associated to the patient and returns
        an instance of Patient

        :patient_dir: The directory containing the "levels_all.csv"
                        and the "activities_all.csv" files.
        :glucose_preprocessor: An instance of Preprocessor suited for the glucose data
        :activity_preprocessor: An instance of Preprocessor suited for the activity data
        :returns: A tuple of dataframes (levels, activities)
        """
        levels = Patient.load_glucose_data(patient_dir, glucose_preprocessor)
        activities = Patient.load_activity_data(patient_dir, activity_preprocessor)

        return Patient(levels, activities)

    def slice(self, window_size):
        """
        Slices the glucose data based on each activity that the patient performs.

        :window_size: The size of the sequence that has to be extracted for each
                    event (in seconds)
        :returns: A dictionary (meal_type) -> [series_1, ..., series_n]
        """
        meal_levels = {}
        # Slice the glucose series by meal
        for row_i, activity in self._activity.iterrows():
            start = activity[Variables.ACTIVITY_TIME_VAR]
            stop = start + Variables.WINDOW_SIZE
            meal_type = activity[Variables.ACTIVITY_MEAL_TYPE]

            if meal_type not in meal_levels:
                meal_levels[meal_type] = []

            series = get_sliced_glucose_level(self._glucose_level, start, stop)
            meal_levels[meal_type].append(series)
        return meal_levels

    def get_activity_elements(self):
        """
        Retrieves a list of elements (consisting in the details of
        each activity) from the activities.

        :text_processor: A Processor that extracts lists from strings
        :returns: A list of lists of elements (one row for each activity)
        """
        foods = []
        for row_i, activity in self._activity.iterrows():
            foods.append([])
            elements = preprocess_text_foods(
                activity[Variables.DESCRIPTION]
            )
            for d in elements:
                if len(d) > 1:
                    # Delete entries with one letter or empty
                    foods[-1].append(d)
        return foods

    def get_summary(self, foods):
        """
        Returns a summary (in a DataFrame) for each event in the list
        of activities.
        The dataframe will include the following columns:
            - Initial time
            - Meal type
            - Initial glucose
            - Final glucose
            - "diff" i.e., final glucose - initial glucose
            - a column for each of the foods ever eaten by the patient
        :foods: A list of lists of foods (a row for each activity)
        :returns: A DataFrame
        """
        patient_df = pd.DataFrame()
        for row_i, (_, activity) in enumerate(self._activity.iterrows()):
            """
            I don't use the indices of the row because the deletion
            of physical activities may affect the order
            """
            if Variables.PHYS_ACTIVITY_STRING in activity[Variables.ACTIVITY_MEAL_TYPE]:
                # skip it, it is not a meal
                continue
            new_row = {}
            new_row[Variables.MEAL_START_TIME] = \
                    activity[Variables.ACTIVITY_TIME_VAR]
            new_row[Variables.MEAL_TYPE] = \
                    activity[Variables.ACTIVITY_MEAL_TYPE]

            start = new_row[Variables.MEAL_START_TIME]
            stop = start + Variables.WINDOW_SIZE
            series = get_sliced_glucose_level(self._glucose_level, start, stop)
            # Take only the glucose level
            glucose_series = series[Variables.GLUCOSE_VAR].values

            new_row[Variables.INITIAL_GLUCOSE] = glucose_series[0]
            new_row[Variables.FINAL_GLUCOSE] = glucose_series[-1]
            new_row[Variables.DIFF] = glucose_series[-1] - glucose_series[0]

            # Normalize w.r.t. the first sample
            norm_gs = glucose_series / glucose_series[0]

            new_row[Variables.MEAN] = norm_gs.mean()

            for f in foods[row_i]:
                new_row[f] = 1

            patient_df = patient_df.append(new_row, ignore_index=True)

        """
        Not the most elegant solution, but saves me another iteration on
          the dataset
        """
        patient_df = patient_df.fillna(0)
        return patient_df
