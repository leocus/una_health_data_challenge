#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    una_health_data_challenge.preprocessing
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    This module contains functions to preprocess dataframes

    :copyright: (c) 2021 by Leonardo Lucio Custode.
    :license: MIT, see LICENSE for more details.
"""
import datetime
import numpy as np
from utils import Variables


class Preprocessor:
    """
    This class implements the abstract class for preprocessing
    classes.
    Implements the "Chain of responsibility" design pattern
    """
    def __init__(self, successor=None):
        """
        Initializes the preprocessor

        :successor: The successor element
        """
        self._successor = successor

    def apply(self, df):
        """
        Performs the preprocessing process on the element and
        hands it to the successor

        :df: A DataFrame
        :returns: A DataFrame
        """
        output = self._apply_preprocessing(df)
        if self._successor is not None:
            output = self._successor.apply(output)
        return output

    def get_successor(self):
        return self._successor

    def set_successor(self, value):
        self._successor = value


class RecordingFilterer(Preprocessor):
    """
    This class filters the rows of the dataset
    by the type of recording
    """

    def __init__(self, rows_to_keep, successor=None):
        """
        Initializes the instance

        :rows_to_keep: A list of id of the measurements
                    types that must be kept.
        :successor: The successor element
        """
        Preprocessor.__init__(self, successor)
        self._rows_to_keep = rows_to_keep

    def _apply_preprocessing(self, df):
        """
        Filters the dataframe by keeping only the records
        that are registered by using the specified types of
        sensors

        :df: A dataframe
        :types: A list of integers representing the types to keep
        :returns: A filtered dataframe
        """
        rows_to_keep = df["Aufzeichnungstyp"].isin(self._rows_to_keep)
        new_df = df[rows_to_keep]
        return new_df


class GlucoseSeriesMerger(Preprocessor):
    """
    This class merges the two series related to
    glucose level in a new timeseries
    """

    def __init__(self, successor=None):
        """
        Initalizes an instance

        :successor: The successor
        """
        Preprocessor.__init__(self, successor)

    def _apply_preprocessing(self, df):
        """
        Merges the glucose-level related variables in a single series
        called "Glucose Level mg/dL"

        :df: A dataframe containing the following series:
            - "Glukosewert-Verlauf mg/dL"
            - "Glukose-Scan mg/dL"
        :returns: the same dataframe with a new series called "Glucose Level mg/dL"
        """
        # Define variables to shorten the code
        gluk_verl = Variables.GLUCOSE_MEASUREMENT
        gluk_scan = Variables.GLUCOSE_SCAN
        new_series = Variables.GLUCOSE_VAR

        df[new_series] = df[gluk_verl].fillna(0) + df[gluk_scan].fillna(0)
        return df


class TimeConverter(Preprocessor):
    """
    This class merges the two series related to
    glucose level in a new timeseries
    """

    def __init__(self, timestamp_var, ymd, divisor, timezone, successor=None):
        """
        Initalizes an instance

        :timestamp_var: name of the column that contains the timestamp
        :ymd: Boolean, True if the date is yyyy-mm-dd, False if it
            is dd-mm-yyyy
        :divisor: String that divides the date from the time
        :timezone: Boolean that indicates whether the timezone is present
        :successor: The successor
        """
        Preprocessor.__init__(self, successor)
        self._timestamp_var = timestamp_var
        self._ymd = ymd
        self._divisor = divisor
        self._timezone = timezone

    def _apply_preprocessing(self, df):
        new_values = df.apply(
            lambda x: self._convert_string_to_seconds(
                x[self._timestamp_var]
            ), axis=1
        )
        df[self._timestamp_var] = new_values
        return df

    def _convert_string_to_seconds(self, string):
        if isinstance(string, float) and np.isnan(string):
            return np.nan

        date, time = string.split(self._divisor)
        a, month, c = list(map(int, date.split("-")))
        time = time.split(":")
        hours = int(time[0])
        mins = int(time[1])
        if len(time) > 2:
            seconds = time[2]
        else:
            seconds = 0
        if self._ymd:
            year = a
            day = c
        else:
            year = c
            day = a
        if self._timezone:
            if "+" in seconds:
                seconds, delay = seconds.split("+")
                # Add the sign back
                delay = "+" + delay
            else:
                seconds, delay = seconds.split("-")
                # Add the sign back
                delay = "-" + delay
            delay_hrs = int(delay)
            hours = (hours + delay_hrs) % 24
        seconds = int(seconds)

        # Transform the date and time in seconds
        seconds = datetime.datetime(
            year, month, day, hours, mins, seconds
        ).timestamp()
        seconds = int(seconds)

        return seconds


class DataFrameSorter(Preprocessor):
    """
    This class sorts the dataframes
    """

    def __init__(self, sorting_var, successor=None):
        """
        Initalizes an instance

        :sorting_var: The variable to sort
        :successor: The successor
        """
        Preprocessor.__init__(self, successor)
        self._sorting_var = sorting_var

    def _apply_preprocessing(self, df):
        return df.sort_values(by=self._sorting_var)


class MealFilterer(Preprocessor):
    """
    This class filters out the activities that do not
    correspond to meals
    """

    def __init__(self, successor=None):
        """
        Initializes an instance

        :successor: The successor
        """
        Preprocessor.__init__(self, successor)
        self._successor = successor

    def _apply_preprocessing(self, df):
        indices = np.logical_not(
            df["record_type"].str.contains("ACTVITY")
        )
        return df[indices]


def get_glucose_processor():
    processors = [
        RecordingFilterer(Variables.RECORDING_TYPES),
        GlucoseSeriesMerger(),
        TimeConverter(Variables.GLUCOSE_TIME_VAR, False, " ", False),
        DataFrameSorter(Variables.GLUCOSE_TIME_VAR)
    ]

    prev = None
    for index, element in enumerate(processors[::-1]):
        element.set_successor(prev)
        prev = element

    return processors[0]


def get_activity_processor():
    processors = [
        MealFilterer(),
        TimeConverter(Variables.ACTIVITY_TIME_VAR, True, "T", True),
        TimeConverter("timestamp_end", True, "T", True),
        DataFrameSorter(Variables.ACTIVITY_TIME_VAR)
    ]

    prev = None
    for index, element in enumerate(processors[::-1]):
        element.set_successor(prev)
        prev = element

    return processors[0]
