#!/usr/bin/env python
"""
    una_health_data_challenge.clustering
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    This script analyzes the data for the una health data challenge.

    :copyright: (c) 2021 by Leonardo Lucio Custode.
    :license: MIT, see LICENSE for more details.
"""
import os
import datetime
import argparse
import numpy as np
import pandas as pd
import seaborn as sns
from copy import deepcopy
from loader import Patient
from utils import Variables
from utils import interpolate
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
import matplotlib.colors as mcolors
from scipy.stats import normaltest, norm
from sklearn.cluster import DBSCAN, KMeans
from preprocessing import get_glucose_processor, get_activity_processor

parser = argparse.ArgumentParser()
parser.add_argument("patient_dir", help="The directory containing the patient's data")
parser.add_argument("n_clusters", type=int, help="The number of clusters")
args = parser.parse_args()
patient_dir = args.patient_dir
gluc_proc = get_glucose_processor()
act_proc = get_activity_processor()
patient = Patient.load_patient_data(patient_dir, gluc_proc, act_proc)
foods = patient.get_activity_elements()
patient_df = patient.get_summary(foods)
meal_levels = patient.slice(Variables.WINDOW_SIZE)


#######################################################################
#                             Clustering                              #
#######################################################################
X = []
y = []

for i, (meal_type, values) in enumerate(meal_levels.items()):
    for series in values:
        X.append(series)
        y.append(meal_type)

# Make all the series of the same length by interpolating
# Basically, what we do here is fixing the time scale with steps of 15 mins
# and then we compute the missing values by interpolating from the nearest data points
time_marks = np.arange(0, Variables.WINDOW_SIZE, Variables.GRANULARITY)
for i, x in enumerate(X):
    X[i] = interpolate(x, time_marks)

# Compute the position of the timeserieses in their underlying vector space
X = np.array(X)

# Cluster
clust = KMeans(n_clusters=args.n_clusters, init="random", random_state=0)
yp = clust.fit_predict(X)
X2D = TSNE(random_state=0).fit_transform(X)

y = np.array(y)
for val in np.unique(yp):
    # Ignore the "Noise" cluster
    if val >= 0:
        print("#######################################################################")
        print("#" + f"Cluster {val}".center(69) + "#")
        print("#######################################################################")
        indices = yp == val
        for meal_type in np.unique(y):
            percentage = np.sum(y[indices] == meal_type)
            percentage /= np.sum(y == meal_type)
            percentage *= 100
            print(f"{meal_type}: {percentage:.2f}%")

print()

#######################################################################
#              Analysis of the foods eaten in each meal               #
#######################################################################
cluster_foods = []

cols = list(patient_df.columns.values)
for c in [
    Variables.MEAL_TYPE,
    Variables.MEAL_START_TIME,
    Variables.INITIAL_GLUCOSE,
    Variables.FINAL_GLUCOSE,
    Variables.DIFF
]:
    cols.remove(c)

for val in np.unique(yp):
    eaten = {}
    indices = np.where(yp == val)[0]

    for c in cols:
        occurrences = patient_df.loc[indices, c].values.sum()
        if occurrences > 0:
            eaten[c] = occurrences / len(indices)
    cluster_foods.append(eaten)

exclusive_foods = []
foods_sets = [set(deepcopy(list(d.keys()))) for d in cluster_foods]
foods_sets_bak = deepcopy(foods_sets)
for i, val in enumerate(np.unique(yp)):
    for j, oth_val in enumerate(np.unique(yp)):
        if val == oth_val:
            continue
        for f in foods_sets_bak[j]:
            assert f in foods_sets_bak[j]
            if f in foods_sets[i]:
                assert f in foods_sets_bak[i]
                foods_sets[i].remove(f)
if sum([len(x) for x in foods_sets]) != 0:
    print("There are unique foods in some clusters:")
    for i, val in enumerate(foods_sets):
        print(i, val)
else:
    print("No unique foods in any cluster")


#######################################################################
#                           Visualization                             #
#######################################################################


for i in range(max(yp)+1):
    plt.figure(figsize=(16,9))
    indices = np.where(yp==i)
    for x in X[indices]:
        plt.plot(x)
    plt.xlabel("Time (15 mins per tick)")
    plt.ylabel(Variables.GLUCOSE_VAR)
    plt.title(f"Cluster {i}")
    plt.tight_layout()
    plt.savefig(os.path.join(patient_dir, f"signals_cluster{i}.pdf"))
    plt.close()

for metric, func in [
        (Variables.MEAN, lambda x: np.mean(x, axis=1)),
        (Variables.DIFF, lambda x: x[:, -1] - x[:, 0])
    ]:
    plt.figure(figsize=(16,9))
    plot = sns.scatterplot(
        x=y,
        y=func(X),
        hue=yp,
        style=y,
        legend="brief",
        s=200,
        palette="deep"
    )
    plt.xlabel("Meal type")
    if metric == Variables.DIFF:
        plt.ylabel(f"Difference in glucose level after {Variables.WINDOW_SIZE/3600:.1f} hours")
    else:
        plt.ylabel(f"Mean glucose level after {Variables.WINDOW_SIZE/3600:.1f} hours")
    # Make 2D legend
    handles, labels = plot.axes.get_legend_handles_labels()
    plot.axes.legend_.remove()
    cluster_legend = plot.legend(handles[:max(yp)+1], labels[:max(yp)+1], title="Clusters", loc="upper left")
    plot.legend(handles[max(yp)+1:], labels[max(yp)+1:], title="Meal type", loc="lower right")
    plt.gca().add_artist(cluster_legend)
    plt.tight_layout()
    plt.savefig(os.path.join(patient_dir, f"clustering_{metric}.pdf"))
    plt.close()

clusters_data = pd.DataFrame()
for c_i, clust in enumerate(cluster_foods):
    for key, val in clust.items():
        temp_dict = {"Id": c_i, "Food": key, "Frequency": val}
        clusters_data = clusters_data.append(temp_dict, ignore_index=True)
clusters_data = clusters_data.fillna(0)
clusters_data = clusters_data.sort_values(by="Food")
division_chars = ["a", "f", "m", "z"]
for i, c0 in enumerate(division_chars[:-1]):
    plt.figure(figsize=(16,9))
    c1 = division_chars[i+1]
    indices = (c0 <= clusters_data["Food"].str[0])
    if sum(indices) == 0:
        continue
    if i < len(division_chars) - 2:
        indices = indices & (clusters_data["Food"].str[0] < c1)
    tmp_cluster = clusters_data[indices]
    if len(tmp_cluster) == 0:
        continue
    plot = sns.barplot(x="Food", y="Frequency", hue="Id", data=tmp_cluster)
    for item in plot.get_xticklabels():
        item.set_rotation(-90)
        plt.tight_layout()
    plt.ylim(0, 1)
    plt.savefig(os.path.join(patient_dir, f"histogram_{c0}-{c1}.pdf"))
    plt.xlabel("Food")
    plt.ylabel("Frequency")
    plt.close()
