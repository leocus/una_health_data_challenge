#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    una_health_data_challenge.una_health_data_challenge
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    This script analyzes the data for the una health data challenge.
    Produces plots that are automatically saved into the execution
    directory.

    :copyright: (c) 2021 by Leonardo Lucio Custode.
    :license: MIT, see LICENSE for more details.
"""
import os
import datetime
import argparse
import warnings
import numpy as np
import pandas as pd
import seaborn as sns
from loader import Patient
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from scipy.stats import normaltest, norm, ttest_ind_from_stats
from utils import Variables, interpolate, get_nearest_glucose_level
from preprocessing import get_glucose_processor, get_activity_processor

parser = argparse.ArgumentParser()
parser.add_argument("patient_dir", help="The directory containing the patient's data")
args = parser.parse_args()
patient_dir = args.patient_dir

gluc_proc = get_glucose_processor()
act_proc = get_activity_processor()
patient = Patient.load_patient_data(patient_dir, gluc_proc, act_proc)
levels = patient.get_glucose_level()
activities = patient.get_activity()
meal_levels = patient.slice(Variables.WINDOW_SIZE)

glucose_time_var = Variables.GLUCOSE_TIME_VAR
glucose_var = Variables.GLUCOSE_VAR
activity_time_var = Variables.ACTIVITY_TIME_VAR


"""
Visualization
Show glucose levels
"""
plt.figure(figsize=(16,9))
plt.plot(levels[glucose_time_var], levels[glucose_var])
plt.xlabel("Time (s)")
plt.ylabel("Glucose level (mg/dL)")
plt.title(f"Patient {patient_dir}")
plt.savefig(os.path.join(patient_dir, "glucose_level.pdf"))
plt.tight_layout()
plt.close()

"""
Glucose levels and meals
Without differentiating between meal types
"""
min_l = levels[glucose_var].min()
max_l = levels[glucose_var].max()
vertical_values = np.arange(min_l, max_l, 1)
plt.figure(figsize=(16,9))
plt.plot(levels[glucose_time_var], levels[glucose_var])
for time in activities[activity_time_var]:
    plt.plot([time] * len(vertical_values), vertical_values, "red")
plt.xlabel("Time(s)")
plt.ylabel("Glucose level")
plt.title(f"Patient {patient_dir}")
plt.savefig(os.path.join(patient_dir, "glucose_level_meals_nodiff.pdf"))
plt.tight_layout()
plt.close()

"""
Glucose levels and meals
Without differentiating between meal types
"""
min_l = levels[glucose_var].min()
max_l = levels[glucose_var].max()
meal_types = activities["record_type"].unique()
colors = mcolors.TABLEAU_COLORS
meal_to_color = {m: c for m, c in zip(meal_types, colors)}

plt.figure(figsize=(16,9))
plt.plot(levels[glucose_time_var], levels[glucose_var], 'k')
handles = []
labels = []
for row_i, activity in activities.iterrows():
    time = activity[activity_time_var]
    meal_type = activity["record_type"]

    # Plot when the meal is eaten
    handle1 = plt.scatter(
        time,
        get_nearest_glucose_level(levels, time),
        c=meal_to_color[meal_type],
        marker=">"
    )
    # Plot the threshold of 3 hours
    three_hrs_later = time + Variables.WINDOW_SIZE
    handle2 = plt.scatter(
        three_hrs_later,
        get_nearest_glucose_level(levels, three_hrs_later),
        c=meal_to_color[meal_type],
        marker="<"
    )

    if meal_type not in labels:
        handles.extend([handle1, handle2])
        labels.append(meal_type)
        labels.append(f"{meal_type} (3 hrs)")
plt.xlabel("Time(s)")
plt.ylabel("Glucose level")
plt.title(f"Patient {patient_dir}")
print(handles, labels)
plt.legend(handles, labels)
plt.savefig(os.path.join(patient_dir, "glucose_level_meals_diff.pdf"))
plt.tight_layout()
plt.close()

for meal_type, values in meal_levels.items():
    for i, series in enumerate(values):
        title = f"{meal_type} n. {i}"
        plt.figure(figsize=(16,9))
        time = series[Variables.GLUCOSE_TIME_VAR].values
        time -= time[0]  # Relative time
        plt.plot(
            time,
            series[Variables.GLUCOSE_VAR].values
        )
        plt.xlabel("Time after meal (s)")
        plt.ylabel("Glucose level")
        plt.title(f"Patient {patient_dir}; {title}")
        plt.savefig(os.path.join(patient_dir, title + ".pdf"))
        plt.tight_layout()
        plt.close()
