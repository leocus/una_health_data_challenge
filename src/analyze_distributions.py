#!/usr/bin/env python
"""
    una_health_data_challenge.analyze_distributions
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    This script analyzes the data for the una health data challenge.

    :copyright: (c) 2021 by Leonardo Lucio Custode.
    :license: MIT, see LICENSE for more details.
"""
import os
import datetime
import argparse
import warnings
import numpy as np
import pandas as pd
import seaborn as sns
from loader import Patient
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from utils import Variables, interpolate
from scipy.stats import normaltest, norm, ttest_ind_from_stats
from preprocessing import get_glucose_processor, get_activity_processor


parser = argparse.ArgumentParser()
parser.add_argument("patient_dir", help="The directory containing the patient's data")
args = parser.parse_args()
patient_dir = args.patient_dir

gluc_proc = get_glucose_processor()
act_proc = get_activity_processor()
patient = Patient.load_patient_data(patient_dir, gluc_proc, act_proc)
foods = patient.get_activity_elements()
patient_df = patient.get_summary(foods)
meal_levels = patient.slice(Variables.WINDOW_SIZE)
print(f"The dataset is composed of {len(patient_df)} samples")

for metric in [Variables.DIFF, Variables.MEAN]:
    print("#"*70)
    print("#" + f"Metric: {metric}".center(68) + "#")
    print("#"*70)

    #######################################################################
    #                        Parameter estimation                         #
    #######################################################################
    # Estimate the distributions of the differences for each meal
    meal_types = patient_df["meal_type"].unique()
    meals_params = {}
    for meal_type in meal_types:
        diffs = patient_df[patient_df["meal_type"] == meal_type][metric].values
        if len(diffs) > 2:
            mu = np.mean(diffs)
            sigma = np.std(diffs)
            meals_params[meal_type] = [mu, sigma, len(diffs)]
            print(f"{meal_type} causes a glucose increase of {mu:.2f}±{sigma:.2f}")


    #######################################################################
    #                         Statistical testing                         #
    #######################################################################
    # Test the distribution to see if we can find significant differences
    #   Since the distributions can be approximated as gaussians, we can
    #   use a simple Welch's T-Test
    print("\nStatistical testing")
    keys = list(meals_params.keys())
    for i1, dist_1 in enumerate(keys):
        for i2, dist_2 in enumerate(keys[:i1]):
            if dist_1 is not dist_2:
                p1 = meals_params[dist_1]
                p2 = meals_params[dist_2]
                _, p = ttest_ind_from_stats(*p1, *p2, equal_var=False)
                if p < 0.05:
                    print(
                        f"{dist_1} is statistically different than {dist_2}"
                    )


    #######################################################################
    #                           Visualizations                            #
    #######################################################################
    # Make a scatter plot with meal types and differences in glucose level
    plt.figure(figsize=(16,9))
    sns.boxplot(x=patient_df["meal_type"], y=patient_df[metric])
    plt.title(f"Glucose difference {Variables.WINDOW_SIZE/3600:.1f} hours after a meal")
    plt.xlabel("Meal type")
    if metric == Variables.DIFF:
        plt.ylabel("Difference")
    else:
        plt.ylabel("Mean normalized value")
    plt.tight_layout()
    plt.savefig(os.path.join(patient_dir, f"boxplot_distributions_{metric}.pdf"))
    plt.close()

    # Plot the distributions (since we cannot perform distribution tests for lists with less than
    #   8 samples, we assume that they are distributed normally
    legend = []
    plt.figure(figsize=(16,9))
    colors = iter(mcolors.TABLEAU_COLORS)
    for meal_type, (mu, sigma, _) in meals_params.items():
        plt.figure(figsize=(16,9))
        if metric == Variables.DIFF:
            bins = [-40, -20, 0, 20, 40]
            plt.xlabel("Difference in glucose level")
        else:
            bins = [0.5, 0.75, 1, 1.25, 1.5]
            plt.xlabel("Mean normalized value in glucose levels")

        plt.hist(patient_df[patient_df["meal_type"] == meal_type][metric], bins, rwidth=0.8)

        plt.title(f"Distribution of the difference in glucose level for {meal_type}")
        plt.ylabel("Occurrences")
        plt.tight_layout()
        plt.savefig(os.path.join(patient_dir, f"{meal_type}_{metric}_hist.pdf"))
        plt.close()

    # Plot the distributions (since we cannot perform distribution tests for lists with less than
    #   8 samples, we assume that they are distributed normally
    legend = []
    plt.figure(figsize=(16,9))
    colors = iter(mcolors.TABLEAU_COLORS)
    for meal_type, (mu, sigma, _) in meals_params.items():
        color = next(colors)
        x = np.linspace(mu - 3*sigma, mu + 3*sigma, 100)  # x-values for a 99% confidence level
        vals = norm.pdf(x, mu, sigma)
        plt.plot(x, vals, color)
        plt.fill_between(x, vals, color=color, alpha=0.3)
        legend.append(meal_type)
    plt.legend(legend)
    plt.title("Distribution of the difference in glucose level for meal type")
    if metric == Variables.DIFF:
        plt.xlabel("Difference in glucose level")
    else:
        plt.xlabel("Mean normalized value in glucose levels")
    plt.tight_layout()
    plt.savefig(os.path.join(patient_dir, f"normal_distributions_{metric}.pdf"))
    plt.close()
