# una_health_data_challenge
A report is available in `report.pdf`
The documentation is produced in the `doc` directory.

To build the visualizations run:
```
python src/visualization.py $PATIENT_DIR
```
To analyze distributions run:
```
python src/analyze_distributions.py $PATIENT_DIR
```
To perform clustering run:
```
python src/clustering.py $PATIENT_DIR $NUM_CLUSTERS
```

All the visualizations are saved in the directory of the patient.
