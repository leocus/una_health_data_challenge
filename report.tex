\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb,mathtools}
\usepackage[detect-weight=true, binary-units=true]{siunitx}
\sisetup{list-final-separator = {, and }}
\usepackage{url,hyperref,cleveref}
\usepackage[linesnumbered]{algorithm2e}
\usepackage{subcaption}
\crefname{algocf}{alg.}{algs.}
\Crefname{algocf}{Algorithm}{Algorithms}
\usepackage[normalem]{ulem} %http://tex.stackexchange.com/a/23712
\usepackage{color,colortbl,etoolbox,xcolor} %http://tex.stackexchange.com/a/2768/22613
\robustify\bfseries
\robustify\uline
\usepackage{multirow,booktabs,makecell,array}
\usepackage{csquotes} %http://tex.stackexchange.com/a/294634
\usepackage[inline]{enumitem}
\usepackage{hhline}
\usepackage{svg}
\usepackage{pgf}
\usepackage{soul}
\usepackage{enumitem}
\usepackage{tablefootnote}
\usepackage{blkarray}
\usepackage{tikz}
\usetikzlibrary{arrows.meta}

\author{Leonardo Lucio Custode}
\title{Una Health Data Challenge - Report}
\date{\today}

\begin{document}
\maketitle

\section{Introduction}
This report describes the approach proposed for the Una Health Data Challenge regarding the following aspects:
\begin{itemize}
    \item Software design;
    \item Methodological approach;
    \item Results.
\end{itemize}

The next Section will describe the structure of the software, Section \ref{sec:approach} describes the approach used to explore the data, and Section \ref{sec:interpretation} shows the outputs and their interpretation.

\section{Software}
\label{sec:sw}
The software is built on top of Python3.8.
The reason underlying this choice relies in the fact that this version of Python has been released in October 2019 \footnote{https://en.wikipedia.org/wiki/History\_of\_Python}, and thus it has been tested by the community for about two years, so its bugs are more likely to be known w.r.t. newer versions.
Moreover, two years of testing seem to be enough to prefer this version to earlier ones.

\subsection{Design}
The software has been designed with two metrics in mind:
\begin{itemize}
    \item Modularity,
    \item Ease of implementation.
\end{itemize}

Modularity is needed because, by having a modular software, we can easily add new functionalities.
Moreover, modularity makes the testing easier.
On the other hand, since I had limited time to implement the software, it was necessary to avoid over-complicated implementations.

For these two reasons, I decided not to adhere to either the fully-functional paradigm or the fully-OOP paradigm.
Instead, by mixing the two paradigms, I was able to produce a modular software, that was also quick to implement.

The software makes use of the following entities:
\begin{itemize}
    \item Patient: contains the data related to the patient and method to transform it
    \item Preprocessor: performs a single preprocessing operation on a dataframe
\end{itemize}

\subsubsection{Patient}
The \textit{Patient} class (located in \texttt{loader.py}) represents a patient and contains all of their data.
It provides, moreover, methods to:
\begin{itemize}
    \item Load patients from a directory
    \item Slice the data
    \item Create a dataset with information for each meal (by processing the descriptions)
\end{itemize}

\subsubsection{Preprocessor}
The \textit{Preprocessor} class (located in \texttt{preprocessing.py}) implements a basic preprocessing unit that implements the ``Chain of responsibility'' design pattern\footnote{https://it.wikipedia.org/wiki/Chain-of-responsibility\_pattern}.

This allows us to easily create pipelines of preprocessors, each of which handles a specific aspect of the data.

\subsection{Libraries}
The libraries used in this project are:
\begin{itemize}
    \item Pandas: to handle dataframes
    \item Matplotlib: to create basic plots
    \item Seaborn: provides a more high-level interface to plotting, useful for more complex plots
    \item Scikit-learn: for the clustering methods
    \item SciPy: to perform statistical testing
\end{itemize}

\subsection{Coding conventions}
The project uses the standard Python coding conventions, made exception for PEP-8, whose adoption would have led to less intelligible variable names.

\section{Methodological approach}
\label{sec:approach}
To explore the data, we first plot the whole time series for each patient.
The time series are shown in Figures \ref{fig:onlyglucose_a}, \ref{fig:onlyglucose_b}, \ref{fig:onlyglucose_c}.

The data including both the glucose level and the meals are shown in Figures \ref{fig:a_nodiff}, \ref{fig:a_nodiff}, \ref{fig:a_nodiff}, \ref{fig:a_diff}, \ref{fig:a_diff}, \ref{fig:a_diff}.

Finally, to by running the \texttt{visualization} script as shown in the README, one can isolate time series for each meal, which are saved in the dir.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/glucose_level.pdf}
    \caption{Glucose level for the first patient}
    \label{fig:onlyglucose_a}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb/glucose_level.pdf}
    \caption{Glucose level for the second patient}
    \label{fig:onlyglucose_b}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{cccccccc-cccc-cccc-cccc-cccccccccccc/glucose_level.pdf}
    \caption{Glucose level for the third patient}
    \label{fig:onlyglucose_c}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/glucose_level_meals_nodiff.pdf}
    \caption{Glucose level for the first patient with the meals}
    \label{fig:a_nodiff}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb/glucose_level_meals_nodiff.pdf}
    \caption{Glucose level for the second patient with the meals}
    \label{fig:b_nodiff}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{cccccccc-cccc-cccc-cccc-cccccccccccc/glucose_level_meals_nodiff.pdf}
    \caption{Glucose level for the third patient with the meals}
    \label{fig:c_nodiff}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/glucose_level_meals_diff.pdf}
    \caption{Glucose level for the first patient with the meals}
    \label{fig:a_diff}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb/glucose_level_meals_diff.pdf}
    \caption{Glucose level for the second patient with the meals}
    \label{fig:b_diff}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{cccccccc-cccc-cccc-cccc-cccccccccccc/glucose_level_meals_diff.pdf}
    \caption{Glucose level for the third patient with the meals}
    \label{fig:c_diff}
\end{figure}

\subsection{Clustering}
By analyzing the normalized average glucose level and the difference between the final glucose value (after 3 hours) and the initial glucose level for each patient, we are able to estimate the distributions of these values, after ensuring that they can be approximated as gaussian distributions as they are not multimodal (statistical tests could not be performed due to a lack of data points).

The data for the normalized mean values are shown in Figures \ref{fig:a_dist},\ref{fig:b_dist},\ref{fig:c_dist}.
Note that the distributions have been computed only for meals that had at least 3 records.
The same data represented with boxplots is shown in Figures \ref{fig:a_boxplots},\ref{fig:b_boxplots},\ref{fig:c_boxplots}.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/normal_distributions_mean.pdf}
    \caption{Normalized glucose level after a meal (w.r.t. to the initial value) (Patient a)}
    \label{fig:a_dist}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb/normal_distributions_mean.pdf}
    \caption{Normalized glucose level after a meal (w.r.t. to the initial value) (Patient b)}
    \label{fig:b_dist}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{cccccccc-cccc-cccc-cccc-cccccccccccc/normal_distributions_mean.pdf}
    \caption{Normalized glucose level after a meal (w.r.t. to the initial value) (Patient c)}
    \label{fig:c_dist}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/boxplot_distributions_mean.pdf}
    \caption{Normalized glucose level after a meal (w.r.t. to the initial value) (Patient a)}
    \label{fig:a_boxplot}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb/boxplot_distributions_mean.pdf}
    \caption{Normalized glucose level after a meal (w.r.t. to the initial value) (Patient b)}
    \label{fig:b_boxplot}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{cccccccc-cccc-cccc-cccc-cccccccccccc/boxplot_distributions_mean.pdf}
    \caption{Normalized glucose level after a meal (w.r.t. to the initial value) (Patient c)}
    \label{fig:c_boxplot}
\end{figure}

\section{Interpretation of the outputs}
\label{sec:interpretation}
From the data previously plotted, we observe the following properties.

\subsection{Patient a}
Dinners, breakfasts and lunches tend to decrease their mean glucose level w.r.t. the initial value.
On the other hand, snacks tend to increase the mean glucose level, while for drinks we have only one data point, so we cannot infer any trend.

\subsection{Patient b}
Breakfasts and dinners tend to increase the mean glucose level in the following 3 hours, while the other meals do not show any particular trend.
For this patient we find a statistical significance between the mean glucose level after dinners and both snacks and lunches.

\subsection{Patient c}
Lunches seem to increase their mean glucose level, while dinners tend to decrease it.
We cannot say anything for the other meals due to lack of data.

\section{Clustering}
We perform a clustering of the interpolated mean glucose levels with a step of 15 minutes for 3 hours.
We use the K-means clustering technique as the only required parameter is the size of the cluster.
In this case, setting the number of clusters is quite straightforward: we can use 3 clusters, aiming to have three different types of data: significant high glucose level, about equal glucose level, significantly low glucose level.

The results of the clusterings are shown in Figures \ref{fig:cluster_a}, \ref{fig:cluster_b}, \ref{fig:cluster_c}.
We can observe that the results of the clusterings are consistent with the hypothesis made earlier, i.e., that they can be clustered w.r.t. their impact on the mean glucose level (even though for patient a the clustering is quite noisy).

Finally, Figures \ref{fig:hist_a_a}, \ref{fig:hist_a_f}, \ref{fig:hist_a_m} show the histograms for the occurrences of each food in the clusters for the first patient.
Similar figures are automatically built for the other patients.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/clustering_mean.pdf}
    \caption{Clusters (Patient a)}
    \label{fig:cluster_a}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb/clustering_mean.pdf}
    \caption{Clusters (Patient b)}
    \label{fig:cluster_b}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{cccccccc-cccc-cccc-cccc-cccccccccccc/clustering_mean.pdf}
    \caption{Clusters (Patient c)}
    \label{fig:cluster_c}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/histogram_a-f.pdf}
    \caption{Histogram for foods (Patient a)}
    \label{fig:hist_a_a}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/histogram_f-m.pdf}
    \caption{Histogram for foods (Patient a)}
    \label{fig:hist_a_f}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/histogram_m-z.pdf}
    \caption{Histogram for foods (Patient a)}
    \label{fig:hist_a_m}
\end{figure}

\end{document}
